# Laboxy API and CLI

This repository contains a `laboxy` library with a partial implementation
of the Laboxy API to create tasks, and a CLI to create tasks from the command-line.

To test, start a server with:
```
nl -l -p 8080
```
While the server is running, run:
```
dune exec cli/main.exe -- -A http://localhost:8080 -U user -P password task --date 2021-09-29 -u user-matricule -c category -n "task name" 50
```
Your server should receive a JSON value.

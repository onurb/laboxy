let (let*) = Lwt.bind
let sf = Printf.sprintf
let return = Lwt.return

type config =
  {
    base_api_url: Uri.t;
    username: string;
    password: string;
  }

type task_value =
  | Mode_1 of { hours: int; minutes: int }
  | Mode_2 of { hours_decimal: float }
  | Mode_3 of { percent: int }

let hours_minutes hours minutes = Mode_1 { hours; minutes }
let hours hours_decimal = Mode_2 { hours_decimal }
let percent percent = Mode_3 { percent }

type task =
  {
    (* When updating tasks, one of the two following IDs must be specified. *)
    laboxy_id: int option;
    external_id: string option;
    year: int; (* e.g. 2021 *)
    month: int; (* 1 to 12 *)
    day: int; (* 1 to 31 *)
    user_matricule: string;
    projects_code: string list;
    task_category: string;
    name: string;
    description: string;
    value: task_value;
    set_user_service: bool; (* only used when updating, if user changed service *)
    set_user_type: bool; (* only used when updating, if user changed type *)
  }

let create_or_update_task config task =
  let url =
    let path =
      let base_path = Uri.path config.base_api_url in
      let base_path =
        if base_path <> "" && base_path.[String.length base_path - 1] = '/' then
          base_path
        else base_path ^ "/"
      in
      base_path ^ "tasks/createUpdate"
    in
    Uri.with_path config.base_api_url path
  in
  let headers =
    let open Cohttp.Header in
    let headers = of_list [ "Content-Type", "application/json" ] in
    add_authorization headers (`Basic (config.username, config.password))
  in
  let body =
    let
      {
        laboxy_id;
        external_id;
        year;
        month;
        day;
        user_matricule;
        projects_code;
        task_category;
        name;
        description;
        value;
        set_user_service;
        set_user_type;
      }
      = task
    in
    let json =
      let opt value name json_of_value =
        match value with
          | None -> []
          | Some value -> [ name, json_of_value value ]
      in
      `O (
        opt laboxy_id "laboxy_id" (fun x -> `Float (float x)) @
        opt external_id "external_id" (fun x -> `String x) @
        ("date", `String (sf "%04d-%02d-%02d" year month day)) ::
        ("user_matricule", `String user_matricule) ::
        (
          match projects_code with
            | [] -> []
            | [ code ] -> [ "projects_code", `String code ]
            | codes -> [ "projects_code", `A (List.map (fun x -> `String x) codes) ]
        ) @
        ("task_category", `String task_category) ::
        ("name", `String name) ::
        ("description", `String description) ::
        (
          "value",
          match value with
            | Mode_1 { hours; minutes } ->
                `O [
                  "mode", `Float 1.;
                  "hours", `Float (float hours);
                  "minutes", `Float (float minutes);
                ]
            | Mode_2 { hours_decimal } ->
                `O [
                  "mode", `Float 2.;
                  "hours_decimal", `Float hours_decimal;
                ]
            | Mode_3 { percent } ->
                `O [
                  "mode", `Float 3.;
                  "percent", `Float (float percent);
                ]
        ) ::
        ("set_user_service", `Bool set_user_service) ::
        ("set_user_type", `Bool set_user_type) ::
        []
      )
    in
    json |> Ezjsonm.value_to_string ~minify:false |> Cohttp_lwt.Body.of_string
  in
  Lwt.catch
    (
      fun () ->
      let* (response, body) = Cohttp_lwt_unix.Client.call ~headers ~body `POST url in
      match response.status with
        | #Cohttp.Code.success_status ->
            let* () = Cohttp_lwt.Body.drain_body body in
            return (Ok ())
        | status ->
            let* body = Cohttp_lwt.Body.to_string body in
            return (
              Error (
                sf
                  "Laboxy responded with %s - %s"
                  (Cohttp.Code.string_of_status status)
                  body
              )
            )
    )
    @@ function
    | Unix.Unix_error (code, _, _) ->
        return (Error (Unix.error_message code))
    | exn ->
        raise exn

let create_or_update_task
    ?laboxy_id
    ?external_id
    ~year
    ~month
    ~day
    ~user_matricule
    ~projects_code
    ~task_category
    ~name
    ~description
    ~value
    ?(set_user_service = false)
    ?(set_user_type = false)
    config =
  let task =
    {
      laboxy_id;
      external_id;
      year;
      month;
      day;
      user_matricule;
      projects_code;
      task_category;
      name;
      description;
      value;
      set_user_service;
      set_user_type;
    }
  in
  create_or_update_task config task
